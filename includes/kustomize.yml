---
################################################################################
#
#   KUSTOMIZE DEPLOYMENT
#
#   Update a kustomization with the tags of images built by the pipeline.
#
#   Environment variables:
#
#       KUSTOMIZE_PATH: the path of the kustomization.yml to update
#       KUSTOMIZE_IMAGE_NAME: the name of the image to update in
#         .images.
#       KUSTOMIZE_MANIFESTS_REPO_URL: the Git repository URL where the
#         Kustomize templates are stored.
#       KUSTOMIZE_MANIFESTS_REPO_KEY: an SSH private key used to authenticate
#         with $KUSTOMIZE_MANIFESTS_REPO_URL. If KUSTOMIZE_MANIFESTS_REPO_KEY
#         is not provied, $KUSTOMIZE_MANIFESTS_REPO_URL is assumed to be HTTPS
#         with the login credentials, if needed.
#
################################################################################
.kustomize-deploy:
  stage: deploy
  image: $OCI_IMAGE_KUSTOMIZE
  retry:
    max: 2
    when:
    - script_failure
  before_script:
  - |-
    export DOCKER_TAG=$(git rev-parse --short HEAD)
    if [ $CI_COMMIT_REF_NAME == $SCM_STABLE_BRANCH ]; then
      export DOCKER_TAG=$(make semantic-version)
    fi
    export GIT_USER_EMAIL=${GIT_USER_EMAIL-"noreply@unimatrixone.io"}
    export GIT_USER_NAME=${GIT_USER_EMAIL-"${CI_PROJECT_TITLE}"}
    export KUSTOMIZE_MANIFEST_DIR="/tmp/manifest"
    export TARGET_ENVIRONMENT=${CI_ENVIRONMENT_SLUG}
    if [ -z ${KUSTOMIZE_PATH+x} ]; then
      echo "Define KUSTOMIZE_PATH";
      env | sort
      exit 1;
    fi
    if [ -z ${KUSTOMIZE_IMAGE_NAME+x} ]; then
      echo "Define KUSTOMIZE_IMAGE_NAME";
      env | sort
      exit 1;
    fi
    if [ -z ${KUSTOMIZE_MANIFESTS_REPO_URL+x} ]; then
      echo "Define KUSTOMIZE_MANIFESTS_REPO_URL";
      env | sort
      exit 1;
    fi
    git config --global user.email "${GIT_USER_EMAIL}";
    git config --global user.name "${CI_PROJECT_TITLE}";
    if [ ! -z ${KUSTOMIZE_MANIFESTS_REPO_KEY+x} ]; then
      chmod 400 $KUSTOMIZE_MANIFESTS_REPO_KEY;
      export GIT_SSH_COMMAND="ssh -i $KUSTOMIZE_MANIFESTS_REPO_KEY -o StrictHostKeyChecking=no"
    fi
    mkdir -p $KUSTOMIZE_MANIFEST_DIR
  script:
  - >-
    git clone --single-branch --branch mainline
    ${KUSTOMIZE_MANIFESTS_REPO_URL} $KUSTOMIZE_MANIFEST_DIR
  - cd $KUSTOMIZE_MANIFEST_DIR
  - >-
    yq w -i $KUSTOMIZE_PATH
    "images(name==${KUSTOMIZE_IMAGE_NAME}).newTag" --style=double
    $DOCKER_TAG
  - >-
    if ! git diff --quiet; then
      git commit -am "Update image tag ($TARGET_ENVIRONMENT)"
      git pull --rebase -X theirs && git push
    fi


.kustomize-deploy-env:
  extends: .kustomize-deploy
  variables:
    KUSTOMIZE_PATH: env/${CI_ENVIRONMENT_TIER}/kustomization.yml
